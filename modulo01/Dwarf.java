
public class Dwarf{
    private String nome;
    private float qtdvida;
    private Status status;
    
    {
       qtdvida = 110.0f; 
       status = status.RECEM_CRIADO;
    }
    
    public Dwarf(String nome){
        this.nome = nome;
        this.status = status;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }    
    
    public String getNome(){
        return this.nome;
    }
    
    public float getQtdVida(){
        return this.qtdvida;
    }
    
    public Status getStatus(){
        return this.status;
    }
    //usar métodos com boolean para verificar e chamar nos ifs
    //ternario comparacao ? verdadeiro : falso
    
    public boolean verificarVida(){
        return this.qtdvida > 0;
    }
    
    public float receberDano(){
        if(verificarVida()){
            this.qtdvida = this.qtdvida >= 10.0f ? this.qtdvida -10f : 0.0f;
            if(!verificarVida()){
                this.status = Status.MORTO; 
            }
        }
        return this.qtdvida;
    }
}
