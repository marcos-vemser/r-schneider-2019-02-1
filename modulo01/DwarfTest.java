

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfTest
{
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf novoAnao = new Dwarf("Torin");
        assertEquals(110.0f, novoAnao.getQtdVida(), .000001);
    }
    
    @Test
    public void dwarfPerde10DeVida(){
        Dwarf novoAnao = new Dwarf("Torin");
        novoAnao.receberDano();
        assertEquals(100.0f, novoAnao.getQtdVida(), .000001);
    }
    
    @Test
    public void dwarfPerdeTodaVida11Ataques(){
        Dwarf novoAnao = new Dwarf("Torin");
        for(int i = 0; i < 11; i++){
            novoAnao.receberDano();
        }
        assertEquals(00.0f, novoAnao.getQtdVida(), .000001);
    }
    
    @Test
    public void dwarfNasceComStatusRecemCriado(){
        Dwarf novoAnao = new Dwarf("Torin");
        assertEquals(Status.RECEM_CRIADO, novoAnao.getStatus());
    }
    
    @Test
    public void dwarfTemStatusAlteradoAoMorrer(){
        Dwarf novoAnao = new Dwarf("Torin");
        for(int i = 0; i < 11; i++){
            novoAnao.receberDano();
        }
        assertEquals(Status.MORTO, novoAnao.getStatus());
    }
    
    @Test
    public void dwarfTemStatusAlteradoSemPerderTodaVida(){
        Dwarf novoAnao = new Dwarf("Torin");
        for(int i = 0; i < 10; i++){
            novoAnao.receberDano();
        }
        assertEquals(Status.RECEM_CRIADO, novoAnao.getStatus());
    }
    
    @Test
    public void dwarfNaoFicaComVidaAbaixoDeZero(){
        Dwarf novoAnao = new Dwarf("Torin");
        for(int i = 0; i < 12; i++){
            novoAnao.receberDano();
        }
        assertEquals(00.0, novoAnao.getQtdVida(), .000001);
    }
}
