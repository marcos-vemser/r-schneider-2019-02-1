
public class Elfo{
    
    private String nome;
    private int experiencia;
    private int indiceFlecha;
    private Inventario itensElfo;
    private Status status;
    
    {
        experiencia = 0;
        indiceFlecha = 1;
        itensElfo = new Inventario();
        status = Status.RECEM_CRIADO;
    }
    
    public Elfo(String nome){
        this.nome = nome;
        this.status = status;
        this.itensElfo.adicionar(new Item (1, "Arco"));
        this.itensElfo.adicionar(new Item (4, "Flecha"));
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }    
    
    public String getNome(){
        return this.nome;
    }
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
   public Item getFlecha(){
       System.out.println(this.itensElfo.obter(indiceFlecha));
        return this.itensElfo.obter(indiceFlecha);
        
    }
    
    public int getQuantidadeFlecha(){
        return this.getFlecha().getQuantidade();
        
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    public void aumentarXP(){
        experiencia++;
    }
    
    public void atirarFlechaDwarf(Dwarf novoAnao){
        System.out.println(this.getFlecha().getQuantidade());
        
        int qtdAtual = this.getFlecha().getQuantidade();
                
        if (qtdAtual > 0){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            novoAnao.receberDano();
            this.aumentarXP();
        }
    }
}
