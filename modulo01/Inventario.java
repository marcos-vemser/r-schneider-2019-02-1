import java.util.*;

public class Inventario{
   ArrayList<Item> inventario;
   // private Item[] itens;
   
   // public Inventario (int quantidade){
       // this.itens = new Item[quantidade];
   // }
   
   // public Inventario (){
       // this(99);
   // }
   
   public Inventario(){
       this.inventario = new ArrayList<Item>();
   }
   
   
   public ArrayList getInventario(){
       return this.inventario;
   }
   
   public void adicionar(Item novoItem){
       inventario.add(novoItem);
   }
   
   public Item obter(int posicao){
      return this.inventario.get(posicao);
  
   }
   
   public void remover(int posicao){
       this.inventario.remove(posicao);
   }
   
   public String getDescricaoItens(){
       String descricaoItem = "";
       for(int k =0; k < inventario.size(); k++){
           if(inventario.get(k) != null){
               descricaoItem += "["+inventario.get(k).getDescricao()+"]";
           }
       }
       return descricaoItem;
   }
   
   //Resolução Método:
   // public String getDescricaoItens2(){
      // StringBuilder descricoes = new StringBuilder();
      // for(int k =0; k < itens.length; k++){
          // Item item = this.itens[k];
          // if(item != null){
          // descricoes.append(item.getDescricao());
          // descricoes.append(",");
          // }
      // }
      // return (descricoes.length() >0 ? descricoes.substring(0, (descricoes.length()-1)): descricoes.toString());
   // }
   
   public Item itemMaiorQuantidade(){
       int auxIndex = 0;
       int auxQtd = 0;
       
       for(int l = 0; l < inventario.size(); l++){
           if(this.inventario.get(l)!=null){
               if(this.inventario.get(l).getQuantidade() > auxQtd){
                   auxQtd = this.inventario.get(l).getQuantidade();
                   auxIndex = l;
               }
           } 
       }
       return this.inventario.size() > 0 ? this.inventario.get(auxIndex) : null; //verifica se tem itens na lista antes de retornar
   }
}
